#!/usr/bin/python3
import argparse
import requests
import sys
import yaml


def setup_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog="update_plan", description="Update the plan file with name identifier"
    )
    group = parser.add_argument_group("Plan Parameters")
    group.add_argument("--plan", help="URL to a Plan yaml file", required=True)
    return parser


def update_plan(plan):
    data = yaml.load(requests.get(plan).text, Loader=yaml.FullLoader)
    for job in data["jobs"]:
        job_name = job["name"]
        builds = job.get("builds", job.get("build"))
        if builds:
            for build in builds:
                build["build_name"] = job_name
        tests = job.get("tests", job.get("test"))
        if tests:
            for test in tests:
                test["test_name"] = job_name
    with open("plan.yml", "w") as f:
        yaml.dump(data, f, default_flow_style=False)


def main() -> int:
    parser = setup_parser()
    options = parser.parse_args()
    update_plan(options.plan)


def start():
    if __name__ == "__main__":
        sys.exit(main())


start()
